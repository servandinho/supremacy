/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////Erroes al momento de cargar la barra/////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function crearPeticion () {
  var peticion = null;
  try {
    peticion = new XMLHttpRequest();
  }catch (IntentarMs) {
    try{
      peticion = new ActiveXObject("Msxml2.XMLHTTP");
    }catch (OtroMs){
      try{
        peticion = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (fallo) {
        peticion = null;
      }
    }
  }
  return peticion;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Barra de Carga///////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

window.onload = function () {
  var archivo = document.getElementById("archivo");
  var barra = document.getElementById("barra");
  archivo.addEventListener("change", function () {
    var informacion = new FormData();
    informacion.append("archivo", archivo.files[0]);
    var peticion = crearPeticion();
    if(peticion == null){
      alert("Tu navegaddor no es compatible");
      return;
    }
    peticion.addEventListener("load", function() {
      //barra.value = "0";
      barra.style.display = "block";

    });
    peticion.upload.addEventListener("progress", function(e) {
      barra.style.display = "block";
      var p = Math.round((e.loaded/e.total)*100);
      barra.value = p;
      barra.innerHTML = p + "%";
      console.log(p);
   });
    peticion.addEventListener("error", function() {
      alert("Error al subir el archivo");
    });
    peticion.addEventListener("abort", function() {
      alert("La subida se aborto, por favor intenta de nuevo");
    });

    });

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Guardar Archivo, boton submit//////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {

  $("#formulario").bind("submit",function(){
      // Capturamnos el boton de envío
          var property = document.getElementById("archivo").files[0];
          var image_name = property.name;
          var submit = document.getElementById("submit");
          var image_extension = image_name.split('.').pop().toLowerCase();
          if (jQuery.inArray(image_extension,  ['png', 'jpg', 'jpeg'])== -1)
            {
              alert("Tu archivo debe ser una imagen");
            }
          else
            {
              var image_size = property.size;
              if(image_size>2000000)
                {
                  alert("La imagen es muy pesada");
                }
              else
                {

                  var form_data = new FormData();
                  form_data.append("archivo", property);
                  $.ajax(
                    {
                      url:"php/subir.php",
                      method:"POST",
                      data:form_data,
                      contentType:false,
                      cache:false,
                      processData:false,
                      beforeSend:function()
                      {
                        $('#respuesta').html("<label class= 'text-success'>Image Uploadding..</label>");
                      },
                      success:function(data)
                      {

                      $('#respuesta').html("<label class= 'text-success'>Imagen Cargada</label>");
                      submit.disabled=true;

                      }
                    });return false;

                  };

              };return false;

  });

});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////Boton Mostrar Más/////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {

  $("#formulario").bind("button",function(){
      // Capturamnos el boton de envío
          var property = document.getElementById("archivo").files[0];
          var image_name = property.name;
          var submit = document.getElementById("submit");
          var image_extension = image_name.split('.').pop().toLowerCase();
          if (jQuery.inArray(image_extension,  ['png', 'jpg', 'jpeg'])== -1)
            {
              alert("Tu archivo debe ser una imagen");
            }
          else
            {
              var image_size = property.size;
              if(image_size>2000000)
                {
                  alert("La imagen es muy pesada");
                }
              else
                {

                  var form_data = new FormData();
                  form_data.append("archivo", property);
                  $.ajax(
                    {
                      url:"php/subir.php",
                      method:"POST",
                      data:form_data,
                      contentType:false,
                      cache:false,
                      processData:false,
                      beforeSend:function()
                      {
                        $('#respuesta').html("<label class= 'text-success'>Image Uploadding..</label>");
                      },
                      success:function(data)
                      {

                      $('#respuesta').html("<label class= 'text-success'>Imagen Cargada</label>");
                      submit.disabled=true;

                      }
                    });return false;

                  };

              };return false;

  });
});
};
