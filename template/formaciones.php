<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <link href="css/formaciones.css" rel="stylesheet">

  <title>Supremacy - Formaciones | Clash Of Clan</title>
  <link rel="shortcut icon" href="images/favicon.ico">
</head>
<body>
  <form id="formulario" enctype="multipart/form-data" method="POST">
  <h3>Seleccionar Imagen</h3>
  <img src="images/formaciones/AdjuntarYa.png" class="image-select">
  <progress value="0" max="100" id="barra">0%</progress>
      <input type="button" value="Adjuntar Imagen" class="load-img">
      <input type="file" name="archivo" id="archivo" class="seleted">
      <input type="submit" value="Cargar" class="up-load" id="submit" disabled="true">
      <p class="respuesta" id="respuesta" value="">
  </form>

  <div class="shape"></div>
  <?php
    $dir = 'images/formaciones/TH_12/';
    // Initiate array which will contain the image name
    $imgs_arr = array();
    // Verifica si el directorio existe
    if (file_exists($dir) && is_dir($dir) ) {

        // Get files from the directory
        $dir_arr = scandir($dir);
        $arr_files = array_diff($dir_arr, array('.','..') ); // elimina los dos primeros elementos del array
        arsort($arr_files); //ordena el array de mayor a menor
        $inicio=0;
        $fin= 9;
        $nueve=9;
        $arr_files = array_slice($arr_files, $inicio, $fin);

        echo '<div class="container-gallery">';
          echo '<h2>Formaciones</h2>';

        echo '<div class="gallery">';

        foreach ($arr_files as $file) {
          //Get the file path
          $file_path = $dir."/".$file;
          // Get extension
          $ext = pathinfo($file_path, PATHINFO_EXTENSION);
          if ($ext=="jpg" || $ext=="png" || $ext=="JPG" || $ext=="PNG" || $ext=="jpeg" || $ext=="JPEG")
          {
            array_push($imgs_arr, $file);
          }

          $count_img_index = count($imgs_arr) - 1;


          echo "<figure><h1>Download</h1><img src='".$dir.$imgs_arr[$count_img_index]."'/></figure>";

        }

        echo '</div>';
        echo '<input type="button" id="mostrar" value="Mostrar Más" class="btn-recient" onclick="mostrar()">';

    }
?>
  <i class="fas fa-plus btn_show"></i>
  </div>

  <script src="js/formaciones.js"></script>

</body>
<script type="text/javascript" src="js/codigo.js"></script>
</html>
